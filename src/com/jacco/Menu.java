package com.jacco;

import java.util.ArrayList;
import java.util.Scanner;

public class Menu
{

    Scanner scanner = new Scanner(System.in);
    private String border = "===============";

    public void loginMenu(List lists)
    {

        final int PATIENT = 1;
        final int ADMIN = 2;
        final int EXIT = 0;

        int choice = 1;

        while (choice != 0)
        {
            System.out.println(border);
            System.out.println("Please enter a digit to log in.");
            System.out.println("[" + EXIT + "] Exit program");
            System.out.println("[" + PATIENT + "] I am a patient");
            System.out.println("[" + ADMIN + "] I am an admin");
            System.out.println(border);

            choice = scanner.nextInt();

            switch (choice)
            {
                case EXIT:
                    break;
                case PATIENT:
                    patientsLoginMenu(lists);
                    break;
                case ADMIN:
                    Admin admin = new Admin(lists);
                    admin.menu();
                    break;
            }

        }

    }

    public void patientsLoginMenu(List lists)
    {

        int choice = 1;

        while (choice != 0)
        {
            ArrayList<Patient> patientList = lists.getPatientList();

            System.out.println(border);
            System.out.println("Please enter your Patient ID");
            System.out.println("Enter 0 to exit this menu");
            System.out.println(border);

            choice = scanner.nextInt();

            if (choice == 0)
            {
                break;
            } else
            {

                for (int i = 0; i < patientList.size(); i++)
                {

                    if (choice == patientList.get(i).getID())
                    {
                        PatientMenu patientMenu = new PatientMenu(patientList.get(i));
                        patientMenu.menu();
                        break;
                    } else
                    {
                        continue;
                    }

                }
            }
        }

    }

}
