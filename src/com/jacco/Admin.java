package com.jacco;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class Admin
{

    Scanner intScanner = new Scanner(System.in);
    Scanner stringScanner = new Scanner(System.in);
    private List lists;
    private String border = "===============";

    Admin(List l)
    {
        lists = l;
    }

    public void menu()
    {

        final int EXIT = 0;
        final int PATIENT_LIST = 1;
        final int MEDICINE_LIST = 2;

        int choice = 1;

        while (choice != 0)
        {
            System.out.println("Welcome Admin (logged in as an admin)");
            System.out.println(border);
            System.out.println("Please enter a digit");
            System.out.println("[" + EXIT + "] Log out");
            System.out.println("[" + PATIENT_LIST + "] View patient list");
            System.out.println("[" + MEDICINE_LIST + "] View medicine list");
            System.out.println(border);

            choice = intScanner.nextInt();

            switch (choice)
            {
                case PATIENT_LIST:
                    viewPatientList();
                    break;
                case MEDICINE_LIST:
                    viewMedicineList();
                    break;
            }

        }
    }

    public void viewPatientList()
    {
        ArrayList<Patient> patientList = lists.getPatientList();

        int choice = 1;

        while (choice != 0)
        {

            System.out.println(border);
            System.out.println("Please enter the ID of a patient to view the data of the patient");
            System.out.println("[0] Go back");

            for (int i = 0; i < patientList.size(); i++)
            {
                System.out.println("[" + patientList.get(i).getID() + "] " + patientList.get(i).getFirstname() + " " + patientList.get(i).getSurname());

            }

            System.out.println(border);

            choice = intScanner.nextInt();

            for (int i = 0; i < patientList.size(); i++)
            {

                if (choice == patientList.get(i).getID())
                {
                    viewPatientData(patientList.get(i));
                    break;
                } else
                {
                    continue;
                }

            }

        }

    }

    public void viewPatientData(Patient patient)
    {
        final int EXIT = 0;
        final int EDIT_DATA = 1;
        final int REMOVE_PATIENT = 2;

        int choice = 1;

        while (choice != 0)
        {
            System.out.println(border);
            System.out.println("Please enter a digit");
            System.out.println("[" + EXIT + "] Go back");
            System.out.println("[" + EDIT_DATA + "] Edit patient data");
            System.out.println("[" + REMOVE_PATIENT + "] Remove patient\n");

            patient.writeData();

            System.out.println(border);

            choice = intScanner.nextInt();

            switch (choice)
            {
                case EDIT_DATA:
                    editPatientData(patient);
                    break;
                case REMOVE_PATIENT:

                    System.out.println("Are you sure you want to remove this patient? Type yes / no");

                    if (stringScanner.nextLine().toLowerCase().equals("yes"))
                    {
                        lists.removePatient(patient);
                        choice = 0;
                    }

                    break;
            }

        }
    }

    public void editPatientData(Patient patient)
    {

        final int EXIT = 0;
        final int CHANGE_FIRSTNAME = 1;
        final int CHANGE_CALLNAME = 2;
        final int CHANGE_SURNAME = 3;
        final int CHANGE_ADDRESS = 4;
        final int CHANGE_DATE_OF_BIRTH = 5;
        final int CHANGE_WEIGTH = 6;
        final int CHANGE_LENGTH = 7;

        System.out.println(border);
        System.out.println("Please enter a digit");
        System.out.println("[" + EXIT + "] Go back");
        System.out.println("[" + CHANGE_FIRSTNAME + "] Change firstname");
        System.out.println("[" + CHANGE_CALLNAME + "] Change callname");
        System.out.println("[" + CHANGE_SURNAME + "] Change surname");
        System.out.println("[" + CHANGE_ADDRESS + "] Change address");
        System.out.println("[" + CHANGE_DATE_OF_BIRTH + "] Change date of birth");
        System.out.println("[" + CHANGE_WEIGTH + "] Change weigth");
        System.out.println("[" + CHANGE_LENGTH + "] Change length");
        System.out.println(border);

        int choice = intScanner.nextInt();

        switch (choice)
        {
            case CHANGE_FIRSTNAME:

                System.out.println(border);
                System.out.println("Please enter the patients new firstname");
                System.out.println(border);

                patient.setFirstname(stringScanner.nextLine());

                break;
            case CHANGE_CALLNAME:

                System.out.println(border);
                System.out.println("Please enter the patients new callname");
                System.out.println(border);

                patient.setCallName(stringScanner.nextLine());

                break;
            case CHANGE_SURNAME:

                System.out.println(border);
                System.out.println("Please enter the patients new surname");
                System.out.println(border);

                patient.setSurname(stringScanner.nextLine());

                break;
            case CHANGE_ADDRESS:

                System.out.println(border);
                System.out.println("Please enter the patients new address");
                System.out.println(border);

                patient.setAddress(stringScanner.nextLine());

                break;
            case CHANGE_DATE_OF_BIRTH:

                System.out.println(border);
                System.out.println("Please enter the patients date of birth (YEAR-MONTH-DAY example: 2012-09-23)");
                System.out.println(border);

                patient.setDateOfBirth(LocalDate.parse(stringScanner.nextLine()));

                break;
            case CHANGE_WEIGTH:

                System.out.println(border);
                System.out.println("Please enter the patients new weith in kg");
                System.out.println(border);

                patient.setWeight(Double.parseDouble(stringScanner.nextLine()));

                break;
            case CHANGE_LENGTH:

                System.out.println(border);
                System.out.println("Please enter the patients new length in meters");
                System.out.println(border);

                patient.setLength(Double.parseDouble(stringScanner.nextLine()));

                break;
        }

    }

    public void viewMedicineList()
    {

    }

}
