package com.jacco;

public class Medicine
{

    private String name;
    private String type;
    private String description;
    private double dosering;

    Medicine(String n, double d, String t, String desc)
    {
        name = n;
        dosering = d;
        type = t;
        description = desc;
    }

}
