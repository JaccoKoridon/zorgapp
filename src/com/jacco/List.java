package com.jacco;

import java.time.LocalDate;
import java.util.ArrayList;

public class List
{

    private ArrayList<Patient> patientList = new ArrayList<>();
    private ArrayList<Medicine> medicineList = new ArrayList<>();

    List()
    {
        addToPatientList(new Patient(patientList.size() + 1,"Jacco", "Koridon", "Fauna 51", 70, 1.83, LocalDate.of(2002, 8, 29)));
        addToPatientList(new Patient(patientList.size() + 1,"Adriaan", "Van Puffelen", "Adres 12", 83, 1.70, LocalDate.of(2000, 2, 29)));
    }

    public void addToMedicineList(Medicine medicine)
    {
        medicineList.add(medicine);
    }

    public ArrayList<Medicine> getMedicineList()
    {
        return medicineList;
    }

    public void addToPatientList(Patient patient)
    {
        patientList.add(patient);
    }

    public ArrayList<Patient> getPatientList()
    {
        return patientList;
    }

    public void removePatient(Patient patient)
    {
        patientList.remove(patient);
    }

}
