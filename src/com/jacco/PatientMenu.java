package com.jacco;

import java.util.Scanner;

public class PatientMenu
{

    Scanner intScanner = new Scanner(System.in);
    Scanner stringScanner = new Scanner(System.in);

    private Patient patient;

    private String border = "===============";

    PatientMenu(Patient p)
    {
        patient = p;
    }

    public void menu()
    {

        final int EXIT = 0;
        final int WRITE_DATA = 1;
        final int CHANGE_CALLNAME = 2;

        int choice = 1;

        while (choice != 0)
        {

            System.out.println("Welcome " + patient.getFirstname() + " (logged in as patient)\n");
            System.out.println(border);
            System.out.println("Please select a digit");
            System.out.println("[" + EXIT + "] Log out");
            System.out.println("[" + WRITE_DATA + "] View your data");
            System.out.println("[" + CHANGE_CALLNAME + "] Change your callname");
            System.out.println(border);

            choice = intScanner.nextInt();

            switch (choice)
            {
                case WRITE_DATA:
                    System.out.println(border);
                    patient.writeData();
                    System.out.println(border + "\n");
                    break;

                case CHANGE_CALLNAME:
                    System.out.println(border);
                    System.out.println("Please enter your new callname");
                    System.out.println(border);
                    patient.setCallName(stringScanner.nextLine());
                    break;
            }

        }

    }

}
