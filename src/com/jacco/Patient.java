package com.jacco;

import java.time.LocalDate;
import java.time.Period;

public class Patient
{

    private int ID;
    private LocalDate dateOfBirth;
    private double weight;
    double length;
    private String surname;
    private String firstname;
    private String callName;
    private String address;

    Patient(int i, String fname, String sname, String addr, double w, double l, LocalDate date)
    {
        ID = i;
        firstname = fname;
        callName = fname;
        surname = sname;
        address = addr;
        weight = w;
        length = l;
        dateOfBirth = date;
    }

    public void writeData()
    {
        System.out.println("Firstname: " + getFirstname());
        System.out.println("Callname: " + getCallName());
        System.out.println("Surname: " + getSurname());
        System.out.println("Address: " + getAddress());
        System.out.println("Date of birth: " + getDateOfBirth());
        System.out.println("Age: " + getAge() + " years");
        System.out.println("Weigth: " + getWeight() + " kg");
        System.out.println("Length: " + getLength() + " meters");
        System.out.print("BMI: ");
        System.out.format("%.1f", calcBMI());
        System.out.println();
    }

    public int getID()
    {
        return ID;
    }

    public String getSurname()
    {
        return surname;
    }

    public void setSurname(String name)
    {
        surname = name;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String name)
    {
        firstname = name;
    }

    public String getCallName()
    {
        return callName;
    }

    public void setCallName(String name)
    {
        callName = name;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String adr)
    {
        address = adr;
    }

    public LocalDate getDateOfBirth()
    {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate date)
    {
        dateOfBirth = date;
    }

    public double getWeight()
    {
        return weight;
    }

    public void setWeight(double w)
    {
        weight = w;
    }

    public double getLength()
    {
        return length;
    }

    public void setLength(double l)
    {
        length = l;
    }

    public double calcBMI()
    {
        return weight / (length * length);
    }

    public int getAge()
    {
        Period age = Period.between(dateOfBirth, LocalDate.now());
        int years = age.getYears();
        return years;
    }

}
